package cand;

import java.util.PriorityQueue;
import java.util.Scanner;

class Places implements Comparable<Places>{
	int index;
	int rank;
	static int allGiven = 0;
	int given = 0;
	public int compareTo(Places cmp){
		if(this.rank > cmp.rank)
			return 1;
		if(this.rank < cmp.rank)
			return -1;
		return 0;
	}
	public Places(int place , int rnkPassed){
		index = place;
		rank = rnkPassed;
	}
	
}

public class Main {
	public static void main(String str[]){
		
		Scanner scn = new Scanner(System.in);
		int N = scn.nextInt();
		Places placesArray[] = new Places[N];
		PriorityQueue<Places> pq = new PriorityQueue<Places>();
		
		for (int i = 0; i < N; i++) {
			int rank = scn.nextInt();
			placesArray[i] = new Places(i,rank);
			pq.add(new Places(i , rank));
		}
		while(!pq.isEmpty())
		{
			Places temp = pq.poll();
			if(placesArray[temp.index].given == 0){
				placesArray[temp.index].given++;
				Places.allGiven++;
			}
			if(
					temp.index > 0 &&
					placesArray[temp.index-1].rank > placesArray[temp.index].rank &&
					placesArray[temp.index-1].given <= placesArray[temp.index].given)
			{
				int diff = (placesArray[temp.index].given - placesArray[temp.index-1].given) +1;
				placesArray[temp.index-1].given = placesArray[temp.index].given+1;
				Places.allGiven += diff;
			}
			if(
					temp.index < N-1 &&
					placesArray[temp.index+1].rank > placesArray[temp.index].rank &&
					placesArray[temp.index+1].given <= placesArray[temp.index].given)
			{
				int diff = (placesArray[temp.index].given - placesArray[temp.index+1].given) +1;
				placesArray[temp.index+1].given = placesArray[temp.index].given+1;
				Places.allGiven += diff;
			}
					
		}
		System.out.println(Places.allGiven);
		
		
		
	}
}
